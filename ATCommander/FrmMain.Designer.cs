﻿namespace ATCommander
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.cboCom = new System.Windows.Forms.ComboBox();
            this.lblComPort = new System.Windows.Forms.Label();
            this.ltCommands = new System.Windows.Forms.ListBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.txtReply = new System.Windows.Forms.TextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.txtCommand = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnParse = new System.Windows.Forms.Button();
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.cboBaud = new System.Windows.Forms.ComboBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.settingSaver = new LittleUmph.GUI.Components.SettingSaver(this.components);
            this.chkClearB4Send = new System.Windows.Forms.CheckBox();
            this.chkSendAfterParse = new System.Windows.Forms.CheckBox();
            this.cboLineEnds = new System.Windows.Forms.ComboBox();
            this.btnHistory = new System.Windows.Forms.Button();
            this.ctxHistory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.hexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lnSwitchAt = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.manualComm2 = new ATCommander.ManualComm();
            this.manualComm1 = new ATCommander.ManualComm();
            ((System.ComponentModel.ISupportInitialize)(this.settingSaver)).BeginInit();
            this.ctxHistory.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboCom
            // 
            this.cboCom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCom.FormattingEnabled = true;
            this.cboCom.Location = new System.Drawing.Point(24, 42);
            this.cboCom.Margin = new System.Windows.Forms.Padding(4);
            this.cboCom.Name = "cboCom";
            this.cboCom.Size = new System.Drawing.Size(169, 30);
            this.cboCom.TabIndex = 0;
            this.cboCom.SelectedIndexChanged += new System.EventHandler(this.cboCom_SelectedIndexChanged);
            // 
            // lblComPort
            // 
            this.lblComPort.AutoSize = true;
            this.lblComPort.Location = new System.Drawing.Point(20, 15);
            this.lblComPort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblComPort.Name = "lblComPort";
            this.lblComPort.Size = new System.Drawing.Size(100, 22);
            this.lblComPort.TabIndex = 1;
            this.lblComPort.Text = "COM Port:";
            // 
            // ltCommands
            // 
            this.ltCommands.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ltCommands.ForeColor = System.Drawing.Color.Crimson;
            this.ltCommands.FormattingEnabled = true;
            this.ltCommands.HorizontalScrollbar = true;
            this.ltCommands.ItemHeight = 22;
            this.ltCommands.Items.AddRange(new object[] {
            "AT",
            "AT-RST",
            "AT-GMR"});
            this.ltCommands.Location = new System.Drawing.Point(24, 80);
            this.ltCommands.Name = "ltCommands";
            this.ltCommands.Size = new System.Drawing.Size(243, 554);
            this.ltCommands.TabIndex = 4;
            this.ltCommands.SelectedIndexChanged += new System.EventHandler(this.ltCommands_SelectedIndexChanged);
            this.ltCommands.DoubleClick += new System.EventHandler(this.ltCommands_DoubleClick);
            // 
            // btnConnect
            // 
            this.btnConnect.Enabled = false;
            this.btnConnect.Location = new System.Drawing.Point(296, 41);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(119, 30);
            this.btnConnect.TabIndex = 5;
            this.btnConnect.Text = "Open";
            this.toolTip.SetToolTip(this.btnConnect, "Open serial port");
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // txtReply
            // 
            this.txtReply.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReply.Location = new System.Drawing.Point(273, 257);
            this.txtReply.Multiline = true;
            this.txtReply.Name = "txtReply";
            this.txtReply.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtReply.Size = new System.Drawing.Size(665, 274);
            this.txtReply.TabIndex = 3;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(421, 41);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(119, 30);
            this.btnRefresh.TabIndex = 5;
            this.btnRefresh.Text = "Refresh";
            this.toolTip.SetToolTip(this.btnRefresh, "Refresh the COM list.");
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // txtCommand
            // 
            this.txtCommand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCommand.Location = new System.Drawing.Point(273, 222);
            this.txtCommand.Name = "txtCommand";
            this.txtCommand.Size = new System.Drawing.Size(505, 30);
            this.txtCommand.TabIndex = 3;
            this.txtCommand.TextChanged += new System.EventHandler(this.SetToolTip);
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.Enabled = false;
            this.btnSend.Location = new System.Drawing.Point(864, 221);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(74, 30);
            this.btnSend.TabIndex = 5;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnParse
            // 
            this.btnParse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnParse.Enabled = false;
            this.btnParse.Location = new System.Drawing.Point(784, 221);
            this.btnParse.Name = "btnParse";
            this.btnParse.Size = new System.Drawing.Size(74, 30);
            this.btnParse.TabIndex = 5;
            this.btnParse.Text = "Parse";
            this.toolTip.SetToolTip(this.btnParse, "Parse the template for input.");
            this.btnParse.UseVisualStyleBackColor = true;
            this.btnParse.Click += new System.EventHandler(this.btnParse_Click);
            // 
            // serialPort
            // 
            this.serialPort.ErrorReceived += new System.IO.Ports.SerialErrorReceivedEventHandler(this.serialPort_ErrorReceived);
            this.serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort_DataReceived);
            // 
            // cboBaud
            // 
            this.cboBaud.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBaud.FormattingEnabled = true;
            this.cboBaud.Items.AddRange(new object[] {
            "9600",
            "19200",
            "38400",
            "57600",
            "74880",
            "115200",
            "230400",
            "460800",
            "921600"});
            this.cboBaud.Location = new System.Drawing.Point(200, 42);
            this.cboBaud.Name = "cboBaud";
            this.cboBaud.Size = new System.Drawing.Size(90, 30);
            this.cboBaud.TabIndex = 7;
            this.toolTip.SetToolTip(this.cboBaud, "Baud rate");
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Location = new System.Drawing.Point(856, 622);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(82, 30);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // settingSaver
            // 
            this.settingSaver.AutoSaverList.Add(this.cboBaud);
            this.settingSaver.AutoSaverList.Add(this.cboCom);
            this.settingSaver.AutoSaverList.Add(this.chkClearB4Send);
            this.settingSaver.AutoSaverList.Add(this.chkSendAfterParse);
            this.settingSaver.AutoSaverList.Add(this.cboLineEnds);
            this.settingSaver.AutoSaverList.Add(this.manualComm1);
            this.settingSaver.AutoSaverList.Add(this.manualComm2);
            this.settingSaver.Form = this;
            this.settingSaver.TopMostCheckBox = null;
            this.settingSaver.TopMostMenuItem = null;
            // 
            // chkClearB4Send
            // 
            this.chkClearB4Send.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkClearB4Send.AutoSize = true;
            this.chkClearB4Send.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkClearB4Send.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkClearB4Send.Location = new System.Drawing.Point(719, 629);
            this.chkClearB4Send.Name = "chkClearB4Send";
            this.chkClearB4Send.Size = new System.Drawing.Size(131, 22);
            this.chkClearB4Send.TabIndex = 8;
            this.chkClearB4Send.Text = "Clear B4 Send";
            this.chkClearB4Send.UseVisualStyleBackColor = true;
            // 
            // chkSendAfterParse
            // 
            this.chkSendAfterParse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkSendAfterParse.AutoSize = true;
            this.chkSendAfterParse.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkSendAfterParse.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSendAfterParse.Location = new System.Drawing.Point(558, 628);
            this.chkSendAfterParse.Name = "chkSendAfterParse";
            this.chkSendAfterParse.Size = new System.Drawing.Size(155, 22);
            this.chkSendAfterParse.TabIndex = 8;
            this.chkSendAfterParse.Text = "Send After Parse";
            this.chkSendAfterParse.UseVisualStyleBackColor = true;
            // 
            // cboLineEnds
            // 
            this.cboLineEnds.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLineEnds.FormattingEnabled = true;
            this.cboLineEnds.Items.AddRange(new object[] {
            "Do Nothing",
            "Send LF",
            "Send CRLF"});
            this.cboLineEnds.Location = new System.Drawing.Point(671, 40);
            this.cboLineEnds.Name = "cboLineEnds";
            this.cboLineEnds.Size = new System.Drawing.Size(161, 30);
            this.cboLineEnds.TabIndex = 10;
            // 
            // btnHistory
            // 
            this.btnHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnHistory.Location = new System.Drawing.Point(273, 623);
            this.btnHistory.Name = "btnHistory";
            this.btnHistory.Size = new System.Drawing.Size(99, 30);
            this.btnHistory.TabIndex = 5;
            this.btnHistory.Text = "&History";
            this.btnHistory.UseVisualStyleBackColor = true;
            this.btnHistory.Click += new System.EventHandler(this.btnHistory_Click);
            // 
            // ctxHistory
            // 
            this.ctxHistory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hexToolStripMenuItem});
            this.ctxHistory.Name = "ctxHistory";
            this.ctxHistory.Size = new System.Drawing.Size(95, 26);
            // 
            // hexToolStripMenuItem
            // 
            this.hexToolStripMenuItem.Name = "hexToolStripMenuItem";
            this.hexToolStripMenuItem.Size = new System.Drawing.Size(94, 22);
            this.hexToolStripMenuItem.Text = "Hex";
            // 
            // lnSwitchAt
            // 
            this.lnSwitchAt.AutoSize = true;
            this.lnSwitchAt.Location = new System.Drawing.Point(838, 49);
            this.lnSwitchAt.Name = "lnSwitchAt";
            this.lnSwitchAt.Size = new System.Drawing.Size(100, 22);
            this.lnSwitchAt.TabIndex = 9;
            this.lnSwitchAt.TabStop = true;
            this.lnSwitchAt.Text = "Switch AT";
            this.lnSwitchAt.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnSwitchAt_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(667, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 22);
            this.label1.TabIndex = 11;
            this.label1.Text = "After Command:";
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescription.Location = new System.Drawing.Point(274, 80);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(665, 135);
            this.txtDescription.TabIndex = 14;
            // 
            // manualComm2
            // 
            this.manualComm2.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manualComm2.Location = new System.Drawing.Point(274, 580);
            this.manualComm2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.manualComm2.Name = "manualComm2";
            this.manualComm2.Size = new System.Drawing.Size(665, 40);
            this.manualComm2.TabIndex = 13;
            this.manualComm2.SendData += new ATCommander.ManualComm.SendDataHandler(this.SendManualData);
            // 
            // manualComm1
            // 
            this.manualComm1.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manualComm1.Location = new System.Drawing.Point(273, 539);
            this.manualComm1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.manualComm1.Name = "manualComm1";
            this.manualComm1.Size = new System.Drawing.Size(665, 40);
            this.manualComm1.TabIndex = 12;
            this.manualComm1.SendData += new ATCommander.ManualComm.SendDataHandler(this.SendManualData);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 664);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.manualComm2);
            this.Controls.Add(this.manualComm1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboLineEnds);
            this.Controls.Add(this.lnSwitchAt);
            this.Controls.Add(this.chkSendAfterParse);
            this.Controls.Add(this.chkClearB4Send);
            this.Controls.Add(this.cboBaud);
            this.Controls.Add(this.ltCommands);
            this.Controls.Add(this.btnHistory);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnParse);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.txtCommand);
            this.Controls.Add(this.txtReply);
            this.Controls.Add(this.lblComPort);
            this.Controls.Add(this.cboCom);
            this.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmMain";
            this.Text = "ATCommander";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.settingSaver)).EndInit();
            this.ctxHistory.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboCom;
        private System.Windows.Forms.Label lblComPort;
        private System.Windows.Forms.ListBox ltCommands;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TextBox txtReply;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.TextBox txtCommand;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnParse;
        private System.Windows.Forms.ToolTip toolTip;
        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.ComboBox cboBaud;
        private LittleUmph.GUI.Components.SettingSaver settingSaver;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnHistory;
        private System.Windows.Forms.ContextMenuStrip ctxHistory;
        private System.Windows.Forms.ToolStripMenuItem hexToolStripMenuItem;
        private System.Windows.Forms.CheckBox chkClearB4Send;
        private System.Windows.Forms.CheckBox chkSendAfterParse;
        private System.Windows.Forms.LinkLabel lnSwitchAt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboLineEnds;
        private ManualComm manualComm1;
        private ManualComm manualComm2;
        private System.Windows.Forms.TextBox txtDescription;
    }
}

