﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using LittleUmph;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using ATCommander.DataTypes;

namespace ATCommander
{
    public partial class FrmMain : Form
    {
        const string STR_DefaultATFile = "ESP AT Commands.json";
        Dictionary<string, ATCommand> _atCommands = new Dictionary<string, ATCommand>();
        List<string> _history = new List<string>();
        dynamic _activeCommand = null;

        public FrmMain()
        {
            InitializeComponent();
        }

        private FileInfo GetAtFile()
        {
            var filePath = Path.Combine(Properties.Settings.Default.AtCommandPath, Properties.Settings.Default.LastATSelection);
            if (File.Exists(filePath))
            {
                return new FileInfo(filePath);
            }

            filePath = Path.GetFullPath(Path.Combine(Properties.Settings.Default.AtCommandPath, STR_DefaultATFile));
            if (File.Exists(filePath))
            {
                return new FileInfo(filePath);
            }

            return null;
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            RefreshComList();
            var atFilePath = GetAtFile();
            if (atFilePath != null && atFilePath.Exists)
            {
                ParseAtCommandList(atFilePath);
            }

            settingSaver.DataStore.Load(cboCom);

            if (cboLineEnds.SelectedIndex == -1)
            {
                cboLineEnds.SelectedIndex = 2;
            }
        }

        private void ParseAtCommandList(FileInfo file)
        {
            Properties.Settings.Default.LastATSelection = file.Name;
            Properties.Settings.Default.Save();

            ltCommands.Items.Clear();

            if (file.Exists)
            {
                string json = File.ReadAllText(file.FullName);
                dynamic atProfile = JObject.Parse(json);
                Text = String.Format("ATCommander - {0}", atProfile.meta.name);

                foreach (dynamic cmd in atProfile.commands)
                {
                    string commandText = cmd.command;
                    if (Str.IsNotEmpty(commandText))
                    {
                        ATCommand atCmd = JsonConvert.DeserializeObject<ATCommand>(cmd.ToString());
                        ltCommands.Items.Add(commandText);
                        _atCommands[commandText] = atCmd;
                    }
                }
            }
        }

        private void RefreshComList()
        {
            cboCom.Items.Clear();
            var ports = SerialPort.GetPortNames();
            foreach (var p in ports)
            {
                cboCom.Items.Add(p);
            }
        }

        private void btnParse_Click(object sender, EventArgs e)
        {
            var frm = new FrmInput(settingSaver, _activeCommand);
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
                txtCommand.Text = frm.GetResult();
                settingSaver.DataStore.RefreshData();

                if (chkSendAfterParse.Checked)
                {
                    btnSend_Click(btnSend, e);
                }
            }
        }

        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var buffer = serialPort.ReadExisting();

            Invoke(new Action(() => {
                txtReply.Text += buffer;
                UI.ScrollToEnd(txtReply);
            }));
        }

        private void serialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            Dlgt.Invoke(txtReply, () =>
            {
                txtReply.Text += "Serial Error: " + e.EventType.ToString() + "\r\n";
            });
        }

        private void ltCommands_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ltCommands.SelectedIndex != -1)
            {
                var selected = ltCommands.SelectedItem.ToString();
                if (_atCommands.ContainsKey(selected))
                {
                    var at = _atCommands[selected];
                    txtCommand.Text = at.Command;
                    txtDescription.Text = at.Description;
                    _activeCommand = at;
                }
            }

        }

        private void SetToolTip(object sender, EventArgs e)
        {
            var c = (Control)sender;
            toolTip.SetToolTip(c, c.Text);

            if (c == txtCommand)
            {
                btnParse.Enabled = btnSend.Enabled = Str.IsNotEmpty(txtCommand.Text);
                btnParse.Enabled = Str.Contains(txtCommand.Text, "$");
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (btnConnect.Text == "Open")
            {
                try
                {
                    serialPort.PortName = cboCom.Text;
                    serialPort.BaudRate = Str.IntVal(cboBaud.SelectedItem.ToString());

                    serialPort.Open();

                    if (serialPort.IsOpen)
                    {
                        btnConnect.Text = "Close";
                        cboCom.Enabled = cboBaud.Enabled = false;

                        txtReply.Text += "Serial opened!\r\n";
                    }
                    else
                    {
                        txtReply.Text += "Something woong!\r\n";
                    }
                }
                catch (Exception xp)
                {
                    LogError(xp.Message);
                }
            }
            else
            {
                try
                {
                    btnConnect.Text = "Open";
                    cboCom.Enabled = cboBaud.Enabled = true;
                    serialPort.Close();
                }
                catch (Exception xp)
                {
                    LogError(xp.Message);
                }
            }
        }

        private void LogError(string errorMessage)
        {
            txtReply.Text += "Error: " + errorMessage + "\r\n";
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            SendData(txtCommand.Text);
        }

        private void SendData(string data)
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(()=>{ SendData(data); }));
                return;
            }

            try
            {
                if (chkClearB4Send.Checked)
                {
                    txtReply.Clear();
                }

                AddToHistory(data);
                var cmd = data.Replace("\\r\\n", "\r\n");

                var endingChars = GetEndingChars();
                serialPort.Write(cmd + endingChars);
            }
            catch (Exception xp)
            {
                LogError(xp.Message);
            }
        }

        private string GetEndingChars()
        {
            if (cboLineEnds.Text.Contains("CRLF"))
            {
                return "\r\n";
            }
            else if (cboLineEnds.Text.Contains("Do Nothing"))
            {
                return "";
            }
            else if (cboLineEnds.Text.Contains("LF"))
            {
                return "\n";
            }
            return "\r\n";
        }

        private void AddToHistory(string command)
        {
            if (Str.IsEmpty(command))
            {
                return;
            }

            if (_history.Contains(command))
            {
                _history.Remove(command);
            }
            _history.Insert(0, command);

            if (_history.Count > 7)
            {
                _history.RemoveAt(_history.Count - 1);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtReply.Clear();
        }
        
        private void btnHistory_Click(object sender, EventArgs e)
        {
            ctxHistory.Items.Clear();
            foreach (var h in _history)
            {
                var itm = new ToolStripMenuItem(h, null, (s, evt) =>
                {
                    txtCommand.Text = ((ToolStripMenuItem)s).Text;
                });
                ctxHistory.Items.Add(itm);
            }

            UI.ShowContext(ctxHistory, btnHistory);
        }

        private void ltCommands_DoubleClick(object sender, EventArgs e)
        {
            ltCommands_SelectedIndexChanged(sender, e);
            if (txtCommand.Text.Contains("$"))
            {
                if (btnParse.Enabled)
                {
                    btnParse_Click(btnParse, EventArgs.Empty);
                }
            }
            else
            {
                btnSend_Click(sender, e);
            }
        }

        private void cboCom_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnConnect.Enabled = Str.IsNotEmpty(cboCom.Text);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshComList();
        }

        private void lnSwitchAt_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            var frm = new FrmATSelection();
            var result = frm.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                if (frm.ATFile != null)
                {
                    ParseAtCommandList(frm.ATFile);
                }
            }
        }

        private void SendManualData(ManualComm sender, string data)
        {
            SendData(data);
        }
    }
}

