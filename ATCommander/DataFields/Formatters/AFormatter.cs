﻿using ATCommander.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATCommander.DataFields.Formatters
{
    /// <summary>
    /// Use this to format the final data, for example convert file to hex.
    /// </summary>
    public abstract class AFormatter
    {
        /// <summary>
        /// Format the value based on the input
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public abstract string FormatValue(string value);
    }

    public class RawFormat : AFormatter
    {
        public override string FormatValue(string value)
        {
            return value;
        }
    }
}
