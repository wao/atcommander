﻿using ATCommander.DataFields.Formatters;
using ATCommander.DataTypes.Validators;
using LittleUmph;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using RequiredEnum = Newtonsoft.Json.Required;

namespace ATCommander.DataTypes
{
    /// <summary>
    /// The AT command structure
    /// </summary>
    public class ATCommand
    {
        /// <summary>
        /// The command template.
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string Command { get; set; }

        /// <summary>
        /// Description or help text
        /// </summary>
        [JsonProperty("desc", Required = Required.Default)]
        public string Description { get; set; }

        /// <summary>
        /// Arguments definitions.
        /// </summary>
        [JsonProperty("args", Required = Required.Default)]
        public Dictionary<string, ATArg> Arguments { get; set; }

        /// <summary>
        /// Indicate weather this command is deprecated.
        /// </summary>
        [JsonProperty("deprecated", Required = Required.Default)]
        public bool Deprecated { get; set; }

        public ATCommand()
        {
            Arguments = new Dictionary<string, ATArg>();
            Deprecated = false;
        }

        /// <summary>
        /// Get the list of all the data field in the AT command text (ie. all the $(param) values)
        /// </summary>
        /// <returns></returns>
        public List<DataField> GetDataFields()
        {
            List<DataField> result = new List<DataField>();

            var atRegex = new Regex(@"\$\((?<name>.+?)\)");
            MatchCollection matches = atRegex.Matches(Command);

            foreach (Match m in matches)
            {
                var fieldName = m.Groups["name"].Value;
                DataField df = null;

                if (Arguments.ContainsKey(fieldName))
                {
                    df = Arguments[fieldName].GetDataField(fieldName);
                }
                else
                {
                    df = new InputField(fieldName, new RawFormat());
                }

                df.ReplacementString = m.Value;
                result.Add(df);
            }
            return result;
        }
    }

    /// <summary>
    /// The argument inside the command text.
    /// </summary>
    public class ATArg
    {
        [JsonProperty("choices", Required = RequiredEnum.Default)]
        public List<dynamic> Choices { get; set; }

        [JsonProperty("validators", Required = RequiredEnum.Default)]
        public List<dynamic> Validators { get; set; }

        [JsonProperty("helpText", Required = RequiredEnum.Default)]
        public string HelpText { get; set; }

        [JsonProperty("required", Required = RequiredEnum.Default)]
        public bool Required { get; set; }

        public ATArg()
        {
            Required = true;
            Choices = new List<dynamic>();
            Validators = new List<dynamic>();
        }

        private bool HasValue(IList list)
        {
            if (list == null || list.Count == 0)
            {
                return false;
            }
            return true;
        }

        public DataField GetDataField(string fieldName)
        {
            DataField df = null;

            if (HasValue(Choices))
            {
                df = new ChoiceField(fieldName, Choices);
            }

            if (!Required)
            {
                df = new InputField(fieldName, new RawFormat());                
            }

            if (df == null)
            {
                foreach (dynamic v in Validators)
                {
                    if (v is String)
                    {
                        if (Str.IsEqual(v, "IP"))
                        {
                            df = new InputField(fieldName, new RawFormat());
                            df.Validators.Add(new ValIp());
                            break;
                        }

                        if (Str.IsEqual(v, "Int"))
                        {
                            df = new InputField(fieldName, new RawFormat());
                            df.Validators.Add(new IntVal());
                            break;
                        }
                    }
                    else
                    {
                        ATVal val = JsonConvert.DeserializeObject<ATVal>(v.ToString());
                        if (val.Int != null)
                        {
                            df = new InputField(fieldName, new RawFormat());
                            df.Validators.Add(new IntVal());
                            df.Validators.Add(new ValRange(val.Int.Min, val.Int.Max));
                            break;
                        }
                    }

                }
            }

            if (df != null)
            {
                df.Argument = this;
                if (Str.IsNotEmpty(HelpText))
                {
                    df.HelpText = HelpText;
                }
                return df;
            }
            return null;
        }
    }

    public class ATVal
    {
        public ATValParam Int { get; set; }
        public ATValParam Decimal { get; set; }
    }

    public class ATValParam
    {
        public decimal Min { get; set; }
        public decimal Max { get; set; }

        public ATValParam()
        {
            Min = Decimal.MinValue;
            Max = Decimal.MaxValue;
        }
    }
}
