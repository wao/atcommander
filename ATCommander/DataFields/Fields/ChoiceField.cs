﻿using ATCommander.DataFields.Formatters;
using LittleUmph;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ATCommander.DataTypes
{
    class ChoiceField : DataField
    {
        // key = the display text, value = the actual value, 
        Dictionary<string, string> _value = new Dictionary<string, string>();

        public ChoiceField(string fieldName, dynamic choices)
            : base(fieldName, new RawFormat())
        {
            var combo = new ComboBox();
            combo.Width = flwInputContainer.Width - DataField.INT_MarginOffset;
            combo.Name = Str.CleanVarName(fieldName);
            combo.DropDownStyle = ComboBoxStyle.DropDownList;

            var defaultValueIndex = 0;
            var curIndex = 0;

            foreach (var c in choices)
            {
                string displayText = "";

                if (c is ICollection)
                {
                    var list = ((JContainer)c).Children().Select(token => token.ToString()).ToArray<string>();
                    _value[list[1]] = list[0];
                    displayText = list[1].ToString();
                }
                else {
                    displayText = c.ToString();
                }

                combo.Items.Add(displayText);
                if (displayText.EndsWith("*"))
                {
                    defaultValueIndex = curIndex;
                }

                curIndex++;
            }

            if (combo.Items.Count > 0)
            {
                combo.SelectedIndex = defaultValueIndex;
            }

            base.InputControl = combo;
            base.flwInputContainer.Controls.Add(base.InputControl);
        }

        public override string GetValue()
        {
            var txt = InputControl.Text;
            var value = "";

            if (_value.ContainsKey(txt))
            {
                value = _value[txt];
            }
            else
            {
                value = txt;
            }

            return base.Formatter.FormatValue(value);
        }

    }
}
