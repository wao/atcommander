﻿using LittleUmph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ATCommander.DataTypes.Validators
{
    public abstract class AValidator
    {
        public abstract string ErrorMessage { get; }

        /// <summary>
        /// Test if the value is valid.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public abstract bool IsValid(string value);
    }    

    public class ValRange : AValidator
    {
        public decimal Min { get; set; }
        public decimal Max { get; set; }

        private decimal _value;

        public override string ErrorMessage
        {
            get
            {
                if (_value == Decimal.MinValue)
                {
                    return "Invalid value";
                }

                if (Min != Decimal.MinValue && Max != Decimal.MaxValue)
                {
                    return String.Format("Your value must be >= {0}, and <= {1}", Min, Max);
                }

                if (Min != Decimal.MinValue)
                {
                    return String.Format("Need to be greater than {0}", Min); 
                }

                if (Max != Decimal.MaxValue)
                {
                    return String.Format("Need to be less than {0}", Max);
                }

                return "";
            }
        }

        public ValRange(decimal min = Decimal.MinValue, decimal max = Decimal.MaxValue)
        {
            _value = Decimal.MinValue;

            Min = min;
            Max = max;
        }

        public override bool IsValid(string value)
        {
            bool success = Decimal.TryParse(value, out _value);

            if (!success)
            {
                return false;
            }
            return _value >= Min && _value <= Max;
        }
    }

    public class ValIp : AValidator
    {
        public override bool IsValid(string value)
        {
            return Regex.IsMatch(value, @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
        }

        public override string ErrorMessage
        {
            get 
            {
                return "Invalid IP address";
            }
        }
    }

    public class IntVal : AValidator
    {
        public override bool IsValid(string value)
        {
            return Str.IsInteger(value);
        }

        public override string ErrorMessage
        {
            get
            {
                return "Must be integer";
            }
        }
    }

    public class DecimalVal : AValidator
    {
        public override bool IsValid(string value)
        {
            return Str.IsDouble(value);
        }

        public override string ErrorMessage
        {
            get
            {
                return "Must be decimal number";
            }
        }
    }
}
